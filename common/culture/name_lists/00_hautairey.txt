﻿name_list_hautairey = {

	cadet_dynasty_names = {
		"dynn_Brong"
	}

	dynasty_names = {
		"dynn_Abarties"
		"dynn_Karisairde"
		"dynn_Juraude"
		"dynn_Tuinde"
		"dynn_Vanaude"
	}

	male_names = {
		Carisaire Bacere Orvale Coime Joire Juraite Causte Alteis Tuine Vaunte Molais Elte Cinet Sunale Eries Gantare Harle Toune Vroile Melore Autege Marice Runo Oscale Orixe Ocie Dauven Voroe Vilis Vauxe
		Canes 
	}

	female_names = {
		
	}
	
	dynasty_of_location_prefix = "dynnp_de"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5
}
