ager_hostility_group = {

	group = "not_creatable"

	akamaras_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 1
			hostility_same_family = 2
			hostility_others = 2
		}
	}
	
	naridis_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 2
			hostility_same_family = 2
			hostility_others = 3
		}
	}
}